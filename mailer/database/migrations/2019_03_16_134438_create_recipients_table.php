<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipients', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('from');

            $table->string('replay_to')->nullable();

            $table->string('address')->index();

            $table->string('subject');

            $table->longText('body');

            $table->string('message_id')->nullabel();

            $table->integer('status_code')->default(100)->index();

            $table->integer('attempted')->default(0);

            $table->json('meta')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipients');
    }
}
