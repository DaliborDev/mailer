<?php

namespace App\Services\Mailer;

use Swift_Mailer;
use Swift_Message;
use Swift_Attachment;
use SendGrid\Mail\Mail;

class MailerService
{
    protected $transport;

    public function __construct(Connector $transport)
    {
        $this->transport = $transport;
    }

    /**
     *  returns a instance of requested
     *  email provider driver
     * 
     * @param string $provider
     * @return class::mixed
     */
    public function getMailerInstance($provider)
    {
        try {

            if(strpos($provider, '_api')) {

                \Log::info('@MailerService preparing SG_Driver');
                //return instance of SendGridAPIdriver
                return $this->transport->createSendGridApiTransport($provider);
            }

            \Log::info('@MailerService preparing SMTP_Driver');
            $smtpTrans = $this->transport->createSwiftTransport($provider);
            //Return instance of the SwiftMailer
            if($smtpTrans) {
                return new Swift_Mailer($smtpTrans);
            }

            return false;
            
        } catch (\Exception $e) {

            \Log::error('Mailer Instace retrival failed, '. $e->getMessage());

            return false;
        }
    }

    /**
     * Gets mailer driver and
     * attempts to send the email tr
     *  
     * @param $message, $provider
     * @return false or number of emails send
     */
    public function sendEmail($message, $provider)
    {
        \Log::info('Sending email trough:  '.$provider);
        //Sent the message trough the swift mailer, return results
        return $this->getMailerInstance($provider)->send($message);
    }

    /**
     * Crate message instance for mailing
     * 
     * @param EmailTransaction $email_transaction
     * @return $message
     */
    public function prepareMessage($email_transaction, $provider)
    {
        if(strpos($provider, '_api')) {
            return $this->prepareSGApiMessage($email_transaction, $provider);

        } else {
            return $this->prepareSwiftMessage($email_transaction, $provider);
        }
    }


    /**
     *  Add headers to swiftmessage
     * 
     * @param SwiftMessage $message 
     * @param Array $headers
     */
    public function addHeaders(&$message, $headers)
    {
        $msgHeaders = $message->getHeaders();

        foreach ($headers as $key => $value) {
            \Log::info('adding header: ' . $key);
            $msgHeaders->addTextHeader($key, $value);
        }
    }

     /**
     *  Add header to swiftmessage
     * 
     * @param SwiftMessage $message
     * @param String $hname, $hvalue
     */
    public function addHeader(&$message, $hname, $hvalue)
    {
        $msgHeaders = $message->getHeaders();

        \Log::info('adding header: ' . $hname);
        $msgHeaders->addTextHeader($hname, $hvalue);

    }

     /**
     *  Add attachemnts to swiftmessage
     *  !!!not tested
     * @param SwiftMessage $message
     * @param String $hname, $hvalue
     */
    public function addAttachments($message, $attachments)
    {
        foreach($attachments as $filename => $url) {
            $message->attach(Swift_Attachment::fromPath($url)->setFilename($filename));
        }
    }

    /**
     * Create SwiftMessage instance
     * from emaildata
     * 
     * @param EmailTransaction $email_data
     * @return SwiftMessage
     */
    private function prepareSwiftMessage($email_data, $provider)
    {
        $message = (new Swift_Message($email_data->subject))
            ->setFrom($email_data->from)
            ->setTo($email_data->to)
            ->setBody($email_data->body, 'text/html');

        $this->addHeaders($message, $email_data->options['headers']);

        //set X-SMTPAPI header if sendgrid provider
        if($provider === 'sendgrid_smtp') {

            $this->addHeader($message, 'X-SMTPAPI', json_encode(
                [
                    "unique_args" => [
                        "mailers_trans_id" => $email_data->message_id,
                    ],
                ]));

        } else if($provider === 'mailjet_smtp') {

            $this->addHeader($message,'X-MJ-CUSTOMID', $email_data->message_id);
        }

        if(isset($email_data->options['attachments'])) {
            $this->addAttachments($message, $email_data->options['attachments']);
        }

        \Log::info('Preapared Message with id: ' .$email_data->message_id);

        return $message;
    }

    
    /**
     * Create Mail instanc
     * from sendgrid package
     * 
     * @param EmailTransaction $email_data
     * @return  SendGrid\Mail\Mail $message
     */
    private function prepareSGApiMessage($email_data, $provider)
    {
        $message = new \SendGrid\Mail\Mail(); 
        
        $message->setFrom($email_data->from);
        
        $message->setSubject($email_data->subject);
        
        $message->addTo($email_data->to);
        
        $message->addContent("text/html", $email_data->body);

        \Log::info('Adding header ID Api header');
        $message->addCustomArgs($email_data->options['headers']);

        return $message;
    }

}
