<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    
    //Handler for sendgrid notifiaction
    public function handleSGwebhook(Request $request)
    {
        try {

            $data = file_get_contents("php://input");
            
            $events = json_decode($data, true);
       
            $provider = 'sendgrid';

            \Log::info($events);
            foreach($events as $event) {
                \dispatch(new \App\Jobs\DeliveryNotificationJob($event, $provider))->onQueue('notifications');               
            }

            return response('', 200);
        
        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return response('', 500);
        }

    }

    //Hendler fo mailjet notification
    public function handleMJwebhook(Request $request)
    {
        try {
            $data = file_get_contents("php://input");
            
            $events = json_decode($data, true);

            $provider = 'mailjet';

            foreach($events as $event) {
                \dispatch(new \App\Jobs\DeliveryNotificationJob($event, $provider))->onQueue('notifications');
            }

            return response('OK', 200);
        
        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return response('Error', 500);
        }

    }
}
