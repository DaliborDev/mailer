<?php

namespace App\Services\Mailer;

use SendGrid;
use Swift_SmtpTransport;

class Connector
{

    /**
     * Create an instance of the SMTP Swift Transport driver.
     *
     * @return \Swift_SmtpTransport
     */
    public function createSwiftTransport($connection)
    {
       try {
            $config = \config('mail_providers.'. $connection);

            \Log::info('Creating transport for '.$connection);

            $transport = new Swift_SmtpTransport($config['host_url'], $config['port']);

            if (isset($config['encryption'])) {
                $transport->setEncryption($config['encryption']);
            }

            if (isset($config['username'])) {
                $transport->setUsername($config['username']);

                $transport->setPassword($config['password']);
            }

            $transport->start();
       
            return $transport;

        } catch (\Swift_TransportException $e) {

            \Log::error('Failed to init Swift Transport '. $e->getMessage());

            return false;
        } 
    }
    
    /**
     * Create an instance of the SendGrid api driver.
     *
     * @return \SendGrid 
     */
    public function createSendGridApiTransport($connection) 
    {
        \Log::info('Creating transport for '.$connection);
        return new \SendGrid(config('mail_providers.'.$connection)['sg_api_key']);
    }
}
