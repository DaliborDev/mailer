<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;

class DeliveryNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $event;

    private $provider;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($event, $provider)
    {
        $this->event    =   $event;

        $this->provider =   $provider;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $header_id = \config('mail_providers.'. $this->provider .'_smtp.unique_id');

       $status_code = $this->parseEvent($header_id);
        
        DB::table('recipients')
            ->where('message_id', $this->event[$header_id])
            ->update(['status_code' =>  $status_code, 'updated_at' => now()]);

        \Log::info('Event from ' . $this->provider .' for msg_id: '. $this->event[$header_id] .' of type: '. $this->event['event'] .'has been provesed' );
    }

    private function parseEvent($header_id) 
    {
        DB::table('notifications')->insertGetId([
            "message_id" => $this->event[$header_id],
            "event"  =>  $this->event['event'],
            "event_data"    =>  json_encode($this->event)
        ]);

        $status_code = 250;

        if(\in_array($this->event['event'], ['bounce', 'blocked', 'dropped', 'deferred'])) {

            $status_code = '500';
        }

        return $status_code;
    }
}
