<?php

/**
 * Mail Providers config file holds the mail SMTP providers
 * reated settings and login data.
 */

return [

    'providers_stack'   => ['sendgrid_smtp','mailjet_smtp','sendgrid_api'],

    "primary_provider"      => env('PRIMARY_MAIL_PROVIDER', 'sendgrid_smtp'),
    "secondary_provider"    => env('SECONDARY_MAIL_PROVIDER', 'mailjet_smtp'),
    "tertiary_provider"     => env('TERTIARY_MAIL_PROVIDER', 'sendgrid_api'),

    "mailjet_smtp" => [

        "default_from"  => env('MAILJET_FROM_EMAIL'),
        "username"      => env('MAILJET_EMAIL_USERNAME'),
        "password"      => env('MAILJET_EMAIL_PASSWORD'),
        "host_url"      => env('MAILJET_EMAIL_HOST_URL'),
        "port"          => env('MAILJET_EMAIL_PORT'),
        "unique_id"     => env('MAILJET_UNIQUE_HEADER'),    
    ],

    "sendgrid_smtp" => [

        "default_from"  => env('SENDGRID_FROM_EMAIL'),
        "username"      => env('SENDGRID_EMAIL_USERNAME'),
        "password"      => env('SENDGRID_EMAIL_PASSWORD'),
        "host_url"      => env('SENDGRID_EMAIL_HOST_URL'),
        "port"          => env('SENDGRID_EMAIL_PORT'),
        "unique_id"     => env('SENDGRID_UNIQUE_HEADER'),
    ],

    "sendgrid_api" => [
        
        "sg_api_key"    =>  env('SENDGRID_API_KEY'),
        'sg_api_url'    =>  env('SECONDARY_QUERY_URL'),
    ]

];
