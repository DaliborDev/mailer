<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\Mailer\MailerService;
use App\Services\Mailer\EmailTransaction;


class EmailSenderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $mailer;

    private $mail_transaction;

    private $provider;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(EmailTransaction $mail_transaction, MailerService $mailer, $provider)
    {
        $this->mailer = $mailer;

        $this->provider = $provider;

        $this->mail_transaction = $mail_transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $providers = \config('mail_providers.providers_stack');
            
            $transport = $this->mailer->getMailerInstance($this->provider);

            while(!$transport) {
                
                \Log::info('Failed to establish connection with mail provider: '. $this->provider);

                $this->provider = array_shift($providers);

                $transport = $this->mailer->getMailerInstance($this->provider);
            }

            //if the transport is established provaider is good
            // if(!$transport) {
                
            //     \Log::info('Failed to establish connection with primery mail provider: '. $this->provider);
            //     $this->provider = \config('mail_providers.secondary_provider');

            //     $transport = $this->mailer->getMailerInstance($this->provider);

            //     if(!$transport) {

            //         \Log::info('Failed to establish connection with secondary mail provider: '. $this->provider);
            //         $this->provider = \config('mail_providers.tertiary_provider');

            //         $transport = $this->mailer->getMailerInstance($this->provider);
            //     }
            // }
            
            $message = $this->mailer->prepareMessage($this->mail_transaction, $this->provider);
        
            $this->mail_transaction->create();
    
            // $result = $this->mailer->sendEmail($message, $this->provider);
            $result = $transport->send($message);

            if($result) {
                $status_code = strpos($this->provider, '_api') ? $result->statusCode() : 250;
            
            } else {
                $status_code = 500;
            }

            $this->mail_transaction->incrementAttempts()->setStatus($status_code);
    

        } catch (Exception $e) {

            \Log::debug('Sending failed @job with: '.$e->getMessage());
        }

    }

    public function failed(Exception $exception) {

        \Log::debug('Sender failed @job with: '.$exception->getMessage());
    }
}
