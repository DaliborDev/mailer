<?php

use Illuminate\Database\Seeder;
use App\Models\StatusCode;

class StatusCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run() {
		
		$codes = json_decode(Storage::get('data/status_codes.json'), true);

		foreach($codes as $code) {

			StatusCode::firstOrCreate(['code' => $code['code'], 'status_type' => $code['status_type']['name']], [

				'code' => $code['code'],

				'name' => $code['name'],

				'label' => $code['label'],

				'success' => $code['success'],

				'description' => $code['description'],

				'status_type' => $code['status_type']['name']
			]);
		}
	}

}
