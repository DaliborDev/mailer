<?php

namespace App\Services\Mailer;

use DB;
use Event;

use App\Jobs\EmailSender;

/**
 * Email Transaction class
 *
 *  Email data container
 *  - implements caching of E-transaction
 *    storage, events and status and retries
 * */
class EmailTransaction
{
    public $to;

    public $from;

    public $body;

    public $replay_to;

    public $message_id;

    public $status_code;

    public $options;

    public $subject;

    private $recipient_id;

    public function __construct($from, $to, $subject, $body, $options = [])
    {
        $this->to       =   $to;
        
        $this->from     =   $from;

        $this->body     =   $body;

        $this->subject  =   $subject;

        $this->message_id   =   \uniqid('mailers_', true);

        $this->options  =   $this->setHeaders($options);   
    }

    /**
     *  create the transactan by inserting in db
     */
    public function create()
    {
        //create recipient
        $this->recipient_id = DB::table('recipients')->insertGetId([
            'address'   =>  $this->to,
            'from'      =>  $this->from,
            'body'      =>  $this->body,
            'subject'   =>  $this->subject,
            'replay_to' =>  $this->replay_to,
            'message_id'    =>  $this->message_id,
            'created_at'    =>  now()
        ]);

        return $this;
    }

    /**
     * Update recipient status
     * 
     * @param int $code
     * @return $this
     */
    public function setStatus($code) {
        
        if(!$this->recipient_id) {
            $this->create();
        }

        if(DB::table('recipients')->whereId($this->recipient_id)->update([ 'status_code' => $code, 'updated_at' => now()])){
            \Log::info('Set mail status code: ' . $code . ' for recipient: ' .$this->recipient_id);

        } else {

            \Log::info('Failed to set status code for recipient: ' .$this->recipient_id);
        }
                
        return $this;
    }

    /**
     * After attempting to send the mail
     * increment the attemped column
     */
    public function incrementAttempts() {

        DB::table('recipients')->whereId($this->recipient_id)->increment('attempted'); 

        return $this;
    }

    /**
     *  Get the email transaction data
     */
    public function getTranAtributes(){

        return \get_object_vars($this);
    }

    /**
     * Set unickue header in the email 
     * transaction object
     * 
     * @param array $options
     * @return array $options
     */
    protected function setHeaders($options)
    {
        $options['headers']['mailers_trans_id'] = $this->message_id;

        return $options;
    }
}
