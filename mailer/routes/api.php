<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/post/mail', 'RequestController@sendEmail');

Route::post('/email/sg/listener', 'NotificationController@handleSGwebhook')->name('webhook-sg-url');

Route::post('/email/ms/listener', 'NotificationController@handleMJwebhook')->name('webhook-mj-url');

//Api Routs

Route::get('/list/emails', 'RequestController@mailList');