<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WebhookNotifiactionsTest extends TestCase
{
    
    public function testSGridWebhookBounce()
    {
        $response = $this->json('POST', '/api/email/sg/listener', json_decode('[{
            "email": "testo@testov.com",
            "event": "bounce",
            "ip": "167.89.106.76",
            "mailers_trans_id": "mailers_5c9b8c8b1c0528.89994425",
            "reason": "550 relay not permitted",
            "sg_event_id": "Ym91bmNlLTAtOTkxODEzMi1aMVVOS3BndVExV2NzRjN0cDFQekJnLTA",
            "sg_message_id": "Z1UNKpguQ1WcsF3tp1PzBg.filter0173p3mdw1-22094-5C9B8CC2-11.0",
            "smtp-id": "<Z1UNKpguQ1WcsF3tp1PzBg@ismtpd0004p1lon1.sendgrid.net>",
            "status": "550",
            "timestamp": 1553697989,
            "tls": 1,
            "type": "blocked"
            }]',true));

        $response->assertStatus(200);
    }
}
