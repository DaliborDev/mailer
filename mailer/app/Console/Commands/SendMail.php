<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Mailer\Connector;
use App\Services\Mailer\MailerService;
use App\Services\Mailer\EmailTransaction;

use App\Jobs\EmailSenderJob;


class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail {from} {to} {subject} {body} {--provider=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send transactional email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //set primary connection
        $provider = \config('mail_providers.primary_provider');

        if($this->argument('provider')) {
            $provider = $this->argument('provider');
        }

        //prepare email transaction data
        $transaction = new EmailTransaction(
            $this->argument('from'),
            $this->argument('to'),
            $this->argument('subject'),
            $this->argument('body')
        );

        //get custom mailer instance
        $mailer = new MailerService(new Connector());

        //dispatch to queue
        \dispatch(new EmailSenderJob($transaction, $mailer, $provider))->onQueue('send-queue');

        // \Log::debug($result);
    }
}
