<?php

namespace Tests\Feature;

use Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SendMailApiTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSendEmail()
    {
        $body = Storage::get("data/template1.html");
        
        $response = $this->json('POST', 'api/post/mail',
         [
            "Transaction" => [
                    "From"=> [
                        "Email" =>  "daliborwork123@outlook.com",
                        "Name"  =>   "Progres Status"
                    ],
                    "To"=>   [
                        [
                            "Email" =>   "d.pesovski@hotmail",
                            "Name"  =>   "Dalibor"
                        ]
                    ],
                    "Subject" => "Trans Email",
                    "Body"=> $body,
                    "Options"   =>   [
                        "headers"   =>   [],
                        "provider"  =>   ""
                    ],
                    "Callback_url" => ""
                ]
            ]);

        $response->assertStatus(200);
    }


    public function testSendMultipleEmails()
    {

        $body = Storage::get("data/template1.html");
        
        $response = $this->json('POST', 'api/post/mail',
         [
            "Transaction" => [
                    "From"=> [
                        "Email" =>  "daliborwork123@outlook.com",
                        "Name"  =>   "Progres Status"
                    ],
                    "To"=>   [
                        [
                            "Email" =>   "d.pesovski@hotmail.com",
                            "Name"  =>   "Dalibor"
                        ],
                        [
                            "Email" =>   "daliboreork123@outlook.com",
                            "Name"  =>   "Dalibor"
                        ],
                        [
                            "Email" =>   "teso@testov.com",
                            "Name"  =>   "Testo Testovski"
                        ]
                    ],
                    "Subject" => "Trans Email",
                    "Body"=> $body,
                    "Options"   =>   [
                        "headers"   =>   [],
                        "delay"     =>   "",
                        "provider"  =>   ""
                    ],
                    "Callback_url" => ""
                ]
            ]);

        $response->assertStatus(200);

    }
}
