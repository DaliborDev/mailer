#!/bin/bash

cd /var/www/html

php artisan db:create

php artisan migrate:fresh --seed

php artisan config:cache

composerd dupautoload