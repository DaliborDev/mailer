# Mailer service

Transactional E-mail service which scalable, easy to setup and use. With this service you can be sure that you email will reach its destination because it work in parallel with multiple SMTP connections, so if the primary SMTP connection is unreachable, it will attempt to connect through the secondary, then tertiary and so on...

Also it has out of the box support for sendgrid and mailjet webhook event notification.

## Installation

The service is designed to be ready to work with just a few simple terminal commands.

1. Clone the mailer project from git clone https://gitlab.com/DaliborDev/mailer.git

2. Run **composer install** in the download directory to install the required packages

3. Set the **.env** vars, there is an .env_default file with full test account setup

4. Build the docker services with **docker-compose build** command 

5. Start the docker services **docker-compose up**

6. Run **php artisan db:init** This custom command will create the MySql database 

7. Run **php artisan migrate --seed**  to create the table structures and seed the status_codes table.

You are all setup to send email, go to http::localhost:8000/mailer to view the panel or go to http::localhost:8000/horizon to check the queued jobs and see the service in action !!

## Configuration

### mail_providers and env
+ The service is implemented with 3 default email provider configuration for SendGrid SMTP, MailJet SMTP and SendGrid Api. The service is not hardcoded to only these providers, you can add more SMTP connections. The connections are defined in the .env file and in the [config\mail_providers.php][1] file.

+ Every connection is defined as primary, secondary, tertiary... so the service will no the order of how to handle fallback connection. 

+ SMTP env configs.
 Th SMTP env configuration does not differ much from the sandgrid smtp configuration, but it has additional configs for the event tracking implementation *<PROVIDER NAME>_UNIQUE_HEADER* and default FROM address *<PROVIDER NAME>_FROM_EMAIL*
```
MAILJET_FROM_EMAIL=
MAILJET_EMAIL_USERNAME=
MAILJET_EMAIL_PASSWORD=
MAILJET_EMAIL_HOST_URL=
MAILJET_EMAIL_PORT=25
MAILJET_UNIQUE_HEADER=CustomID
```
## Service

+ For making the service scalable for all load heavy operations as sending emails and receiving webhook notifications we use laravel queued jobs [EmailSenderJob.php][2] and [DeliveryNotificationJob.php][3]

+ The service can be called through CLI command and API json request.

+ API route /api/post/email expects json data formatted in the following structure:
``` json
{
    Transaction: {
        From: {
            Email: '',
            Name: ''
        },
        To: [
            {
                Email: '',
                Name: ''
            }
        ],
        Subject: '',
        Body: '',
        Options: {
            headers: []
            provider: ''
        }
    }
}
```

- From  - is an object with the email address and name of the sender
- To    - is an array of object with the destination email address and name, supports multiple objects, it will send separate emails to all To objects
- Subject   -   Email subject
- Body  - email body, html or plain text expected here
- Options - is and object were optional data is sent such as additional mail headers or choosing default provider

###Files

The mailer service core is comprised of 3 files:

- [Connector.php][4] The connector class initiates the sender transport driver with Swift_SmtpTransport class or Sendgrid api instance
- [EmailTransaction.php][5] EmailTransaction class is responsible for handling transaction data such as, creating transaction, assigning unique message id, set status code, increment attempts.
- [MailerService.php][6] the MailerService class prepares the message for sending and uses the Connector class to make the connection and send the email

The fallback handling is done in the EmailSenderJob for optimizing the server load, this way the job can attempt to connect to the primary provider, if fails can try the secondary and so on...

This code provides the fallback implementation:
``` php

    $providers = \config('mail_providers.providers_stack');
    
    $transport = $this->mailer->getMailerInstance($this->provider);

    while(!$transport) {
        
        \Log::info('Failed to establish connection with mail provider: '. $this->provider);

        $this->provider = array_shift($providers);

        $transport = $this->mailer->getMailerInstance($this->provider);
    }
```

##Docker

 + Services implemented are dedicated to only one purpouse and can scale for procesing many emails at a time. Sevices in docker-compose configuration are: apache, php, horizon, mysql

## Notification

Mailer Service supports webhook notification from sendgrid and mailjet event webhooks through the post routes:
``` php
Route::post('/email/sg/listener'
Route::post('/email/mj/listener'
```
all the events that arrive on this route are parsed and stored in the notification table which is linked to the recipients tabel through the unique message_id column. And the recipient status_code is updated.


[1]: https://gitlab.com/DaliborDev/mailer/blob/master/mailer/config/mail_providers.php "mail_providers"
[2]: https://gitlab.com/DaliborDev/mailer/blob/master/mailer/app/Jobs/EmailSenderJob.php "EmailSender"
[3]: https://gitlab.com/DaliborDev/mailer/blob/master/mailer/app/Jobs/DeliveryNotificationJob.php "event webhook handler" 
[4]: https://gitlab.com/DaliborDev/mailer/blob/master/mailer/app/Services/Mailer/Connector.php "connector"
[5]: https://gitlab.com/DaliborDev/mailer/blob/master/mailer/app/Services/Mailer/EmailTransaction.php "Email Data"
[6]: https://gitlab.com/DaliborDev/mailer/blob/master/mailer/app/Services/Mailer/MailerService.php "mailer"