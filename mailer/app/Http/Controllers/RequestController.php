<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Services\Mailer\Connector;
use App\Services\Mailer\MailerService;
use App\Services\Mailer\EmailTransaction;

use App\Models\Recipient;

use App\Jobs\EmailSenderJob;

class RequestController extends Controller
{

    /**
     *  Return the recipients list for front end display
     * 
     * @return json
     */
    public function mailList(Request $request)
    {
        $skip = 0;

        $limit = 20;
        
        $list = Recipient::select(
            'address', 
            'from', 
            'status_code', 
            'updated_at',
            'created_at',
            'attempted',
            'meta'
            );

        if($request->has('address')) {
            $list->whereIn('address', explode(',', $request->address));
        }

        if($request->has('status_code')) {
            $list->whereIn('status_code', $request->status_code);
        }

        if($request->has('limit')) {
            $limit = $request->limit;
        }

        if($request->has('skip')) {
            $skip   =   $request->skip;
        }

        $list->limit($limit)->skip($skip)->orderBy('created_at', 'desc');

        return response()->json($list->get());
    }
    
    /**
     *  send email transaction data to queued job 
     * 
     * @param json transaction data
     * @return array queued_ids if succes, 
     *  array failed_ids if failed
     */
    public function sendEmail(Request $request)
    {

        Validator::make($request->all(), [
            "Transaction.From.Email"    =>  "required|email",
            "Transaction.To.*.Email"    =>  "required|email",
            "Transaction.Subject"       =>  "required",
            "Transaction.Body"          =>  "required",
        ])->validate();

        $message_ids = [];

        try {

            //set default connection
            $provider = \config('mail_providers.primary_provider');

            //if provider is provided in json object set it as primary
            if(isset($request->Transaction['Options']['provider'])) {

                if($request->Transaction['Options']['provider'] != ''){
                    $provider = $request->Transaction['Options']['provider'];
                }
            }

            foreach($request->Transaction['To'] as $recipient) {
            
                //prepare email transaction data
                $transaction = new EmailTransaction(
                    $request->Transaction['From']['Email'],
                    $recipient['Email'],
                    $request->Transaction['Subject'],
                    $request->Transaction['Body']
                );

                $message_ids[] = $transaction->message_id;

                //get custom mailer instance
                $mailer = new MailerService(new Connector());

                //dispatch to queue
                \dispatch(new EmailSenderJob( $transaction, $mailer, $provider))->onQueue('send-queue');

            }

            return response()->json(['queued_ids' => $message_ids], 200);

        } catch(Exception $e) {

            \Log::error('Request failed: '. $e->getMessage());

            return response()->json(['failed_ids' => $message_ids], 500);
        }

    }

}
